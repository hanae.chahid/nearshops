const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
//import user model
const User = require('../models/user');

//import input validation
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');

module.exports = {
    register: function(req, res){
        const { errors, isValid } = validateRegisterInput(req.body);
        // Check Validation
        if (!isValid) {
            return res.status(400).json(errors);
        }
        User.findOne({ email: req.body.email})
            .then(user =>{
                if(user){
                    errors.email = 'Email already exists';
                    res.status(400).json(errors);
                }else{
                    const   newUser = new User(req.body);
                    //first, we should hash the user's password
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(newUser.password, salt, (err, hash) => {
                            if(err) throw err;
                            //store hash in our password db
                            newUser.password = hash;
                            //then save our new user
                            newUser.save()
                            .then( user =>  res.status(201).json({ user: user}))
                            .catch( err => res.status(500).json(err));
                        })
                    })
                }
            })
    },

    login: function (req, res){
        const { errors, isValid } = validateLoginInput(req.body);
        // Check Validation
        if (!isValid) {
            return res.status(400).json(errors);
        }
        User.findOne({ email: req.body.email})
            .then( user => {
                if(!user){
                    errors.email ='this email not found!'
                    return  res.status(404).json(errors);
                }else{
                    //check user password
                    const password = req.body.password;
                    bcrypt.compare(password, user.password)
                          .then( (isMatch) => {
                                if(isMatch){
                                    jwt.sign({id: user._id}, process.env.JWT_SECRET, (err, token)=> {
                                        if(err) throw err;
                                        return res.status(201).json({ user: user, jwt: 'Bearer ' + token});
                                    });
                                    
                                }else{
                                    errors.password = 'password incorrect!';
                                    return res.status(400).json(errors);
                                }
                            })
                }
            })
    }
}
