const validateRegisterInput = require('../validation/register');


module.exports = {
    validationInput: function(req, res){
        const { errors, isValid } = validateRegisterInput(req.body);
        // Check Validation
        if (!isValid) {
            return res.status(400).json(errors);
        }
    }
}
        