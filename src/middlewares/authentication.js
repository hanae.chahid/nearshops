const passport = require('passport');

module.exports = {
    authentication: function(req, res, next){
        passport.authenticate('jwt',{ session: false, function(err, user, info){
            if(err){
                return next(err);
            }else{
                if(!user){
                    return next( res.json('unautorized!'));
                }else{
                    req.user = user
                    return next();
                }
            }
        }
    })(req, res, next)
    }
}