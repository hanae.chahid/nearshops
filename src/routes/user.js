const express = require('express');
const router = express.Router();

//import controllers
const { register, login} = require('../controllers/user.controller');

//import middlewares
const {authentication} = require('../middlewares/authentication');

/**
 * @route api/users/register
 * @desc  register a new user
 * npm install -g --production windows-build-tools
 */
router.post('/register', register)

/**
 * @route api/users/login
 * @desc  login user
 */
router.post('/login', login)
/**
 * @route api/users/current
 * @desc current user
 */
router.get('/current',authentication, function(req, res){
    res.json({ user: req.user});
});

module.exports = router