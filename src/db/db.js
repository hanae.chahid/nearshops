const mongoose = require('mongoose');
const config = require('../config/config');

const connectString =  `mongodb://${config.db.host}: ${config.db.port}/${config.db.name}`;
module.exports = {
   connection: function(){
     //connect to mongodb  
     mongoose.connect(connectString, {useNewUrlParser: true,useCreateIndex: true})
            .then(()=> console.log('MongoDB connected'))
            .catch( err => console.log(err));
   } 
}



