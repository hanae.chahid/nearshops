const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
     ExtractJwt = require('passport-jwt').ExtractJwt;
//load user model
const User = require('../models/user');

const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
}

const strategy = new JwtStrategy(opts, async function (jwt_payload, done) {
    const user = await User.findOne(jwt_payload._id);
    try{
        if(user) {
            return done(null, user);
        }else {
            return done(null, false);
        }
    }catch( err ){
        return done(res.status(500).json(err), false);
    }
})

passport.use(strategy);