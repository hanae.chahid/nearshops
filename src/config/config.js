const config = {
    app: {
      port: 5000
    },
    db: {
      host: 'localhost',
      port: 27017,
      name: 'shops_db'
    }
};


module.exports = config;