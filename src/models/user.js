const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//creat a user schema
const userSchema = new Schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true}
});

const User = mongoose.model('User', userSchema);


module.exports = User;