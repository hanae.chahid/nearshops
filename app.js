const express = require('express');
const mongoose = require('mongoose');
const config = require('./src/config/config');
const bodyParser = require('body-parser');
const passport = require('passport');
require('dotenv').config();
const userRoutes = require('./src/routes/user');

// create and setup express app
const app = express();

//body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//passport middleware
app.use(passport.initialize());

//config passsport
require('./src/config/passport');

//handle error middleware
app.use(errorHandler);

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
})

//connect to our MongoDB
require('./src/db/db').connection();

//use routes
app.use('/api/users', userRoutes);

//handler error
function errorHandler (err, req, res, next) {
    return  res.status(500)
               .json({
                    error: err
            });     
}


app.listen(config.app.port, () =>{
    console.log('listening on port ' + config.app.port);
});